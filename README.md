# Unity Challenge: 



## Description



## Notice


## Requirement
The application currently works only on Windows 64bits plateform.

## Play The Application
* First download the build from the [release page]().
* Launch the build and Enjoy! 

## Building
The application does not support resizing window. Indeed only the canvas will
resize but not the gameobjects. Thus, the option **Resizable Window** needs to be
uncheked. The resolution need to be fixed and standard (4k HUD,QHD,FullHD,WXGA).

## Code Documentation
The documentation of the code can be found [here]()


>
> *- Thibault Porssut*