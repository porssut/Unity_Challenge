using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{

    float speed = 20.0f;
     float score;
    Vector3 jumpDirection;
    float jumpForce;
    // Start is called before the first frame update
    void Start()
    {
        jumpDirection = new Vector3(0.0f, 2.0f, 0.0f);
        jumpForce = 2.0f;
        score = 0;


    }

    // Update is called once per frame
    void Update()
    {
        TranslateCube();
        RotateCube();
        Jump();
    }


    void TranslateCube()
    {
        
         if (Input.GetKey("up")&& !Input.GetKey(KeyCode.LeftControl))
         {
             transform.Translate(Vector3.forward * Time.deltaTime);
             
         }

          if (Input.GetKey("down")&& !Input.GetKey(KeyCode.LeftControl))
         {
             transform.Translate(Vector3.back * Time.deltaTime);
             
         }

          if (Input.GetKey("right")&& !Input.GetKey(KeyCode.LeftControl))
         {
             transform.Translate(Vector3.right * Time.deltaTime);
             
         }

          if (Input.GetKey("left")&& !Input.GetKey(KeyCode.LeftControl))
         {
             transform.Translate(Vector3.left * Time.deltaTime);
             
         }

    }


    void RotateCube()
    {
        if (Input.GetKey("up") && Input.GetKey(KeyCode.LeftControl))
         {
            transform.RotateAround(transform.position, Vector3.up, speed * Time.deltaTime);
             
             
         }

          if (Input.GetKey("down")&& Input.GetKey(KeyCode.LeftControl))
         {
            transform.RotateAround(transform.position, Vector3.down, speed * Time.deltaTime);
             
         }

          if (Input.GetKey("right")&& Input.GetKey(KeyCode.LeftControl))
         {
            transform.RotateAround(transform.position, Vector3.left, speed * Time.deltaTime);
        }

          if (Input.GetKey("left")&& Input.GetKey(KeyCode.LeftControl))
         {
            transform.RotateAround(transform.position, Vector3.back, speed * Time.deltaTime);
            
             
         }   
    }

    void Jump()
    {
          if (Input.GetKey(KeyCode.Space))
          {
            GetComponent<Rigidbody>().AddForce(jumpDirection*jumpForce,ForceMode.Impulse);
            Debug.Log("tets");
        }

    }

    public void addScore()
    {
        score += 1;
    }
}
