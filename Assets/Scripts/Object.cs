using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider player)
    {
        
        if(gameObject.tag=="collectible")
        {
            Destroy(gameObject);
            player.GetComponent<MovementController>().addScore();
        }
    }
}
